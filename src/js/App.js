import React from 'react';
import Nav from '../JS/Nav';
import About from '../JS/About';
import Code from '../JS/Code';
import Contact from '../JS/Contact'
import Footer from '../JS/Footer'
import Home from '../JS/Home'
import '../CSS/App.css'
import{
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from 'react-router-dom';
import forms from '@material-ui/core'


function App() {
    return (
        <Router>
            <div className='App'>
                <Nav />
                <Switch>
                    <Route path = "/"exact component = {Home}/>
                    <Route path = "/about" exact component = {About}/>
                    <Route path = "/code" exact component = {Code}/>
                    <Route path = "/contact" exact component = {Contact}/>
                </Switch>
                <Footer />
            </div>
        </Router>
    );
}


export default App;
