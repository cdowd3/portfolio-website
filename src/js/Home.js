import React from 'react';
import '../CSS/Home.css';



function Home() {
    return (
        <div className= 'home'>
            <h1>Home Page Description</h1>
            <div className={'home-container'}>
                <div className={'home-description'}>
                    <h3>Christopher G. Dowd</h3>
                    <p>A little description about myself. I am a senior computer science student attending the
                        University of New Haven. I am looking for a position as an entry level software engineer at
                        a defense contracting company such as General Dynamics or Lockheed Martin. I served four
                        years in the United States Navy as an Aviation Electrician and know my experiences and
                        technical skills would translate effectively into a role at these companies.
                    </p>
                </div>
                <div className={'home-picture'}>
                    <h3>Picture</h3>
                    <img src={'https://ih1.redbubble.net/image.641418225.7320/flat,750x,075,f-pad,750x1000,f8f8f8.u3.jpg'}/>
                </div>
            </div>
        </div>
    );
}

export default Home;