import React from 'react';
import '../CSS/Footer.css';



function Footer() {
    return (
        <div className={'page-container'}>
            <div className={'content-wrap'}>
            </div>
            <footer className={'footer'}>
                <div className={'author'}>
                    <p>Author: Christopher G. Dowd<br></br>
                        cdowd3@unh.newhaven.edu </p>
                </div>
                <div className={'copyright'}>
                    <p>Copyright 2020</p>
                </div>
            </footer>
        </div>
    );
}

export default Footer;