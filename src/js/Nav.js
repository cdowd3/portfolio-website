import React from 'react';
import '../CSS/Nav.css'

import {Link} from "react-router-dom";
import {BottomNavigation} from "@material-ui/core";
import About from "./About";

const navStyle = {
    color: 'white'
}

function Nav() {
    return (
        <nav>
            <h3>Christopher G. Dowd</h3>
            <ul className = "nav-links">
                <Link style={navStyle} to={'/'}>
                    <li>Home</li>
                </Link>
                <Link style={navStyle} to={'/about'}>
                    <li>About</li>
                </Link>
                <Link style={navStyle} to={'/code'}>
                    <li>Code</li>
                </Link>
                <Link style={navStyle} to={'/contact'}>
                    <li>Contact</li>
                </Link>
            </ul>
        </nav>

    );
}

export default Nav;