import React from 'react';
import '../CSS/Code.css'



function Code() {

    return (
        <div className={'code-container'}>
            <h1>Code Page</h1>
            <div className={'code-description'}>
                <p>This is one part of an example program (CarList.cpp) written in c++ that represents a linked list data structure.
                    This program resembles a car rental business that utilizes two linked lists to insert cars
                    into a list that includes a make, model, year and type. Once the car list has been created
                    a user can view the list of cars and select one to rent. The car will then be removed from
                    the list. The administrator within the program has the ability to view, delete and add cars
                    to the list.
                </p>
                <div className={'code-box'}>
                    <div className={'code picture'}>
                            <pre>{`
                                <code>
                            <#include "CarList.hpp"
                            #include "CarObj.hpp"
                            #include "Node.h"
                            #include <iostream>
                            #include <string.h>
                            using namespace std;


                            void CarLinkedList::MakeEmpty() {
                            Node* temp = nullptr;
                            while (head != nullptr) {
                            temp = head;
                            head = head->next;
                            delete temp;
                        }
                            length = 0;
                        }

                            void CarLinkedList::PutItem(CarObj car) {
                            Node* temp = new Node;
                            temp->data = car;
                            temp->next = head;
                            head = temp;
                            length++;
                        }

                            CarObj CarLinkedList::GetItem(CarObj car) {
                            Node* temp = head;
                            while (temp != nullptr) {
                            if (temp->data == car) {
                            return temp->data;
                        }
                            temp = temp->next;
                        }
                            throw "not found";
                        }

                            void CarLinkedList::DeleteItem(CarObj car) {
                            // Empty case
                            if (head == nullptr)
                            return;

                            Node* temp;
                            // Deleting head
                            if (head->data == car) {
                            temp = head->next;
                            delete head;
                            head = temp;
                            length--;
                            return;
                        }

                            // Deleting further in the list
                            temp = head;
                            while (temp->next != nullptr) {
                            if (temp->next->data == car) {
                            Node* temp2 = temp->next;
                            temp->next = temp->next->next;
                            delete temp2;
                            length--;
                            return;
                        }
                            temp = temp->next;
                        }
                        }

                            void CarLinkedList::ResetList() {
                            currentPos = head;
                        }

                            CarObj CarLinkedList::GetNextItem() {
                            if (currentPos == nullptr)
                            throw "Out of range";

                            CarObj data = currentPos->data;
                            currentPos = currentPos->next;
                            return data;
                        }

                            int CarLinkedList::GetLength() {
                            return length;
                        }

                            CarLinkedList::~CarLinkedList() {
                            MakeEmpty();
                        }

                            ostream& operator << (ostream& os, CarLinkedList& o) {
                            o.ResetList();
                            for (int i = 0; i < o.GetLength(); ++i) {
                            os << "(" << o.GetNextItem() << ")";
                            if (i < o.GetLength() - 1) os << ",\n";
                        }
                            os << "]\n";
                            return os;
                        }>
                        </code>
                            `}</pre>
                    </div>
                    <div className={'code-right'}>
                        <h1>Right</h1>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Code;